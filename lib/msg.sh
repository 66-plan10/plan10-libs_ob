#!@BINDIR@/bash
# Copyright (c) 2015-2020 Eric Vidal <eric@obarun.org>
# All rights reserved.
# 
# This file is part of Obarun. It is subject to the license terms in
# the LICENSE file found in the top-level directory of this
# distribution.
# This file may not be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#
# msg.sh - functions for outputting messages

##		Shell color 

# Reset
Reset='\033[0m'       # Text Reset
Bold='\033[1m'

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Bold
BBlack='\033[1;30m'       # Black
BRed='\033[1;31m'         # Red
BGreen='\033[1;32m'       # Green
BYellow='\033[1;33m'      # Yellow
BBlue='\033[1;34m'        # Blue
BPurple='\033[1;35m'      # Purple
BCyan='\033[1;36m'        # Cyan
BWhite='\033[1;37m'       # White

# Underline
UBlack='\033[4;30m'       # Black
URed='\033[4;31m'         # Red
UGreen='\033[4;32m'       # Green
UYellow='\033[4;33m'      # Yellow
UBlue='\033[4;34m'        # Blue
UPurple='\033[4;35m'      # Purple
UCyan='\033[4;36m'        # Cyan
UWhite='\033[4;37m'       # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White

# High Intensty
IBlack='\033[0;90m'       # Black
IRed='\033[0;91m'         # Red
IGreen='\033[0;92m'       # Green
IYellow='\033[0;93m'      # Yellow
IBlue='\033[0;94m'        # Blue
IPurple='\033[0;95m'      # Purple
ICyan='\033[0;96m'        # Cyan
IWhite='\033[0;97m'       # White

# Bold High Intensty
BIBlack='\033[1;90m'      # Black
BIRed='\033[1;91m'        # Red
BIGreen='\033[1;92m'      # Green
BIYellow='\033[1;93m'     # Yellow
BIBlue='\033[1;94m'       # Blue
BIPurple='\033[1;95m'     # Purple
BICyan='\033[1;96m'       # Cyan
BIWhite='\033[1;97m'      # White

# High Intensty backgrounds
On_IBlack='\033[0;100m'   # Black
On_IRed='\033[0;101m'     # Red
On_IGreen='\033[0;102m'   # Green
On_IYellow='\033[0;103m'  # Yellow
On_IBlue='\033[0;104m'    # Blue
On_IPurple='\033[10;95m'  # Purple
On_ICyan='\033[0;106m'    # Cyan
On_IWhite='\033[0;107m'   # White

# Compatibilities with old version
reset=${Reset}
bold=${Bold}
red=${Red}
bred=${BRed}
green=${Green}
bgreen=${BGreen}
yellow=${Yellow}
byellow=${BYellow}
blue=${Blue}
bblue=${BBlue}

out_debug() {
	if (( $DEBUG )); then
		printf "%s" " ${FUNCNAME[3]}: " >&1
 	fi
}
out() {
	local errno="${1}" color="${2}" msg="${@:3}"
	printf "${0##*/}:$(out_debug)${color}%s${Reset}: %b\n" "${errno}" "$msg" >&1
}
out_void() {
	printf "\n" 
}
out_menu_title() {
	printf "%b\n" "${BBlue}${@}${Reset}" >&1
}
out_menu_list() {
	printf "%b\n" "${Bold}${@}${Reset}" >&1
}
out_trace() {
	out "tracing" "${Bold}" "${@}"
}
out_success() {
	local msg="${@}"
	out "success" "${BGreen}" "${@}"
}
out_warn(){
	out "warning" "${BYellow}" "${@}"
}
out_error() {
	out "fatal" "${BRed}" "${@}"
}
out_answer() {
	out "Please answer" "${BYellow}" "y or n"
}
out_info() {
	out "info" "${BBlue}" "${@}"
}


info() {
	printf "%b\n" "${@}${reset}" >&1
}

info_void() {
	out_void
}

info_bold() {
	info "${Bold}" "${@}"
}

info_blue() {
	info "${BBlue}" "${@}"
}

info_yellow() {
	info "${BYellow}" "${@}"
}

info_red() {
	info "${BRed}" "${@}"
}

info_green() {
	info "${BGreen}" "${@}"
}


# Compatibilities
out_notvalid() {
	out_warn "${@}"
}

out_action() {
	out_trace "${@}"
}

out_valid() {
	out_success "${@}"
}


