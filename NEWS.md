Changelog for obarun-libs

---

# In 0.2.1

- die function return 111 instead of 1

---

# In 0.2.0

- Pass to oblog program

---

# In 0.1.8

- Pass to new color function

---

# In 0.1.7
	
- rewritte lib/msg.sh

---
	
# In 0.1.6
	
- Force to populate keyring
- Install package after a build even on same version

---
	
# In 0.1.5

- Bugs fix
- Allow to specify branch to use on pac_update

---

# In 0.1.4
	
- Bugs fix
- Install missing dependencies at package update
- use group wheel instead of SUDO_OWNER

---
	
# In 0.1.3

- Pass under ISC license
- Add makefile
- pac.sh : check_version func : compare tag and commit separately and install only if differs. Use git describe instead of git rev.
