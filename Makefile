# Makefile for obarun-libs

VERSION = $$(git describe --tags| sed 's/^v//;')
PKGNAME = obarun-libs

BINDIR = /usr/bin

SCRIPTS = $$(find lib/ -type f)

install: 
	
	install -Dm 0755 util.sh $(DESTDIR)/usr/lib/obarun/util.sh && \
	sed -i 's,@BINDIR@,$(BINDIR),' $(DESTDIR)/usr/lib/obarun/util.sh
	
	for i in $(SCRIPTS); do \
		install -Dm 0755 $$i $(DESTDIR)/usr/lib/obarun/$$i; \
		sed -i 's,@BINDIR@,$(BINDIR),' $(DESTDIR)/usr/lib/obarun/$$i; \
	done
	
	install -Dm644 LICENSE $(DESTDIR)/usr/share/licenses/$(PKGNAME)/LICENSE

version:
	@echo $(VERSION)
	
.PHONY: install version 
