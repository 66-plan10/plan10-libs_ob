Common bash functions utilities for Obarun distributions scripts. 

# License

Under ISC license

# Contact information

* Email:
  Eric Vidal `<eric@obarun.org>`

* Web site:
  https://web.obarun.org/

* XMPP Channel:
  obarun@xmpp.obarun.org
  
